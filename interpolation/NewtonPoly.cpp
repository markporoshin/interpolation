//
// Created by Марк on 17.02.19.
//

#include "NewtonPoly.hpp"

NewtonPoly::NewtonPoly(ifstream &in) {
    in >> N;
    xh = (double *)(malloc(sizeof(double) * N));
    yh = (double *)(malloc(sizeof(double) * N * N));
    ah = (double *)(malloc(sizeof(double) * N));


    for (int i = 0; i < N; ++i)
        in >> xh[i] >> yh[(i) * N];


    setDivSub();
    //printYh();
    //printAh();
}

void NewtonPoly::addNode(double x, double y)  {
    N += 1;
    yh = (double *)realloc(yh, sizeof(double) * (N) * (N));
    yh[(N-1) * N] = y;
    for (int i = 1; i < N; ++i) {
        yh[ (N-1-i) * N + i] = yh[ (N-i) * N + (i-1)] - yh[ (N-1-i) * N + (i-1)];
    }


    xh = (double *)realloc(xh, sizeof(double) * N);
    xh[N-1] = x;


    ah = (double *)realloc(ah, sizeof(double) * N);
    ah[N-1] = yh[N-1] * getW(N-1, xh[N-1], N);

}

double NewtonPoly::getW(int k, double x, int M)  {
    double w = 1;
    for (int i = N-1; i > N-1-M; --i) {
        if (i != k)
            w *= x - xh[i];
    }
    return w;
}

double NewtonPoly::getValue(double x)  {
    double y = 0;
    for(int i = 0; i < N; i++) {
        y += ah[i] * getW(N, x, 0);
    }
    return y;
}

NewtonPoly::~NewtonPoly()  {
    free(xh);
    free(yh);
    free(ah);
}

void NewtonPoly::writeTo(ofstream &out) {
    out << N << endl;
    for (int i = 0; i < N; ++i) {
        out << ah[i] << ' ';
    } out << endl;
}

void NewtonPoly::writeTo(FILE * out) {
    fprintf(out, "%d\n", N);
    for (int i = 0; i < N; ++i) {
        fprintf(out, "%.15lf ", ah[i]);
    } fprintf(out, "\n");
}

void NewtonPoly::setDivSub()  {
    for (int i = 1; i < N; ++i) {
        for (int j = 0; j < N-i; ++j) {
            yh[j *  N + i] = (yh[(j+1) *  N + i-1] - yh[(j) *  N + i-1]) / (xh[j+i] - xh[j]);
        }
    }

    for (int i = 0; i < N; ++i) {
        ah[i] = yh[(N-1-i) * N + i];
    }
}

void NewtonPoly::printAh()  {
    for (int i = 0; i < N; ++i) {
        printf("%lf ", yh[(N-i-1) * N + i]);
    } printf("\n");
    fflush(stdout);
}

void NewtonPoly::printYh() {
    for (int i = 0; i < N; ++i, printf("\n")) {
        for (int j = 0; j < N; ++j) {
            printf("%lf ", yh[i * N + j]);
        }
    }
}