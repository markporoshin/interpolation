//
// Created by Марк on 17.02.19.
//

#ifndef NUMMETHODS2_NEWTON_POLY_HPP
#define NUMMETHODS2_NEWTON_POLY_HPP

#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

struct NewtonPoly {
    
    
    
    NewtonPoly(ifstream & in);

    void addNode(double x, double y);

    double getW(int k, double x, int M);

    double getValue(double x);

    void writeTo(ofstream &out);

    void writeTo(FILE * out);

    void printAh();

    void printYh();

    ~NewtonPoly();


    
    



    void setDivSub();

    int N;
    double * xh;
    double * yh;
    double * ah;
};


void newton_start();
bool check(NewtonPoly & poly, double (*fun)(double x));
double fun(double x);

#endif //NUMMETHODS2_NEWTON_POLY_HPP
