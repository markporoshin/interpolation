//
// Created by Марк on 09.03.19.
//

#include <cmath>
#include "NewtonPoly.hpp"



inline bool isZero(double x) {
    static double E = 1e-13;
    return fabs(x) < E;
}

bool check(NewtonPoly & poly, double (*fun)(double)) {
    for (int i = 0; i < poly.N; ++i) {
        if (!isZero(poly.getValue(poly.xh[i]) - fun(poly.xh[i])))
            return false;
    }
    return true;
}


double fun(double x) {
    return pow(2, sin(x));
}


void newton_start() {
    ifstream in("/Users/mark/CLionProjects/NumMethods2/in.txt");
    FILE * out = fopen("/Users/mark/CLionProjects/NumMethods2/out.txt", "w");

    NewtonPoly poly(in);
    poly.writeTo(out);

    in.close();
    fclose(out);


    std::cout << check(poly, fun);
}