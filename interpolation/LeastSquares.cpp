//
// Created by Марк on 09.03.19.
//

#include <cmath>
#include "LeastSquares.hpp"
#include "../matrix/matr.hpp"
#include "../matrix/solve_g.h"

using namespace std;
Grid::~Grid() {
    delMtr(xh);
    delMtr(yh);
}
Grid::Grid(ifstream &in) {
    in >> M;
    xh = createMtr(M, 1);
    yh = createMtr(M, 1);
    for (int i = 0; i < M; ++i) {
        in >> *mtr_pel(xh, i, 0) >> *mtr_pel(yh, i, 0);
    }
}
FuncBase::FuncBase(ifstream & in) {in >> N;}
string FuncBase::printF(int ind) { return string();}
TrigBase::TrigBase(ifstream & in) : FuncBase(in) {}
string TrigBase::printF(int index) {
    if (index == 0)
        return  string("1");
    else if (index % 2 == 1) {
        string res(("cos(") + to_string((index+1) / 2) + string(" * x)"));// + string(" * x)");
        return res;
    }
    else {
        string res(("sin(") + to_string((index+1) / 2) + string(" * x)"));// + string(" * x)");
        return res;
    }
}
ExpBase::ExpBase(ifstream & in) : FuncBase(in) {}
double TrigBase::value(int index, double x) {
    if (index == 0)
        return 1;
    else if (index % 2 == 1)
        return cos((index+1) / 2 * x);
    return sin((index+1) / 2 * x);
}
double ExpBase::value(int index, double x) {
    if (index == 0)
        return 1;
    else if (index % 2 == 1)
        return exp((index+1) / 2 * x);
    return exp(-(index+1) / 2 * x);
}
int TrigBase::getN() { return N;}
int ExpBase::getN() { return N;}
LeastSquares::LeastSquares(FuncBase *base, Grid *grid) : base(base), grid(grid) {
    initSLAE();
    countCoef();
}
double LeastSquares::getValue(double x) {
    double y = 0;
    for (int i = 0; i < base->getN(); ++i) {
        y += mtr_el(b, i, 0) * base->value(i, x);
    }
    return y;
}
mtr_t * LeastSquares::getValue(mtr_t * x) {
    mtr_t * y = createMtr(x->rows, x->columns);
    for (int i = 0; i < x->rows; ++i) {
        *mtr_pel(y, i, 0) = getValue(mtr_el(x, i, 0));
    }
    return y;
}
void LeastSquares::initSLAE() {
    A = createMtr(base->getN(), base->getN());



    mtr_t * P = createMtr(grid->M, base->getN());
    for (int i = 0; i < grid->M; ++i) {
        for (int j = 0; j < base->getN(); ++j) {
            *mtr_pel(P, i, j) = base->value(j, mtr_el(grid->xh, i, 0));
        }
    }

    //writeTo(stdout, P);
    mtr_t * P_T = trans(P);
    //writeTo(stdout, P_T);
    mtrMul(P_T, P, A);
    //writeTo(stdout, A);

    b = createMtr(base->getN(), 1);
    mtrMul(P_T, grid->yh, b);
    //writeTo(stdout, b);

    delMtr(P);
    delMtr(P_T);
}
void LeastSquares::countCoef() {
    solve_g(A, b);
    //writeTo(stdout, b);
}
LeastSquares::~LeastSquares() {
    delMtr(A);
    delMtr(b);
}
void LeastSquares::printA(ofstream &out) {
    out << b->rows << ' ' << b->columns << endl;
    for (int i = 0; i < b->rows; ++i) {
        out << mtr_el(b, i, 0) << ' ' << endl;
    }
}
void LeastSquares::printInter() {
    for (int i = 0; i < base->getN(); ++i) {
        cout << mtr_el(b, i, 0) << " * " << base->printF(i) << ((i < (base->getN()-1)) ? " + " : "");
    } cout << endl;
}