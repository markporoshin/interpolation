//
// Created by Марк on 09.03.19.
//

#ifndef NUMMETHODS2_LEASTSQUARES_HPP
#define NUMMETHODS2_LEASTSQUARES_HPP

#include <iostream>
#include <fstream>
#include "../matrix/matr.hpp"

using namespace std;

//interface for base set of functions
class FuncBase {
protected:
    int N; //number of func
public:
    FuncBase(ifstream & in);
    virtual int getN() = 0;
    virtual double value(int index, double x) = 0;
    virtual string printF(int ind);
};
//implementation trig base of functions
class TrigBase : public FuncBase {
public:
    TrigBase(ifstream & in);
    int getN();
    double value(int index, double x) override;
    string printF(int ind);
};
class ExpBase : public FuncBase {
public:
    ExpBase(ifstream & in);
    int getN();
    double value(int index, double x) override;
};

struct Grid {
    int M;
    mtr_t * xh;
    mtr_t * yh;

    Grid(ifstream & in);
    ~Grid();
};
class LeastSquares {
    mtr_t * A;
    mtr_t * b;
    FuncBase * base;
    Grid * grid;

    void countCoef();
    void initSLAE();
public:
    LeastSquares(FuncBase * base, Grid * grid);
    double getValue(double x);
    mtr_t * getValue(mtr_t * x);
    void printA(ofstream & out);
    void printInter();
    ~LeastSquares();
};


void least_start();


#endif //NUMMETHODS2_LEASTSQUARES_HPP
