//
// Created by Марк on 09.03.19.
//

#include <cmath>
#include "LeastSquares.hpp"


using namespace std;


//y = f(x);
double f(double x) {
    //this is suitable function for basis 1, cos(nx), sin(nx)
    return exp(sin(x));
}

void initInFILE(string name, double (*fun)(double), string funDescription) {
    double a, b;
    int N, M;
    cout << "researching function: " << funDescription << endl;
    cout << "input limits:" << endl;
    cin >> a >> b;
    cout << "input number of nodes:" << endl;
    cin >> N;
    cout << "input number of functions:" << endl;
    cin >> M;
    ofstream intxt(name);
    intxt << M << ' ' << N-- << endl;
    for (int i = 0; i <= N; i++) {
        double x = a + (b - a) * i / (N);
        intxt << x << ' ' << fun(x) << endl;
    }
    intxt.flush();
    intxt.close();
}







void least_start() {
    initInFILE("/Users/mark/CLionProjects/NumMethods2/in.txt", f, "exp(sin(x))");
    std::ofstream out("/Users/mark/CLionProjects/NumMethods2/out.txt");
    std::ifstream in("/Users/mark/CLionProjects/NumMethods2/in.txt");
    TrigBase base(in); //read number of function
    Grid     grid(in); //read number of node and the nodes
    LeastSquares ls(&base, &grid);
    //mtr_t * x = readFrom(in);
    //mtr_t * y = ls.getValue(x);

    //ls.printA(out);
    //writeTo(out, y);

    ls.printInter();

    //delMtr(x);
    //delMtr(y);

}