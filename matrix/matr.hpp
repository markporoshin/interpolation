//
// Created by Марк on 12.12.18.
//

#ifndef NUMERICALMETHOD_4_SCALARMULTI_MATR_H
#define NUMERICALMETHOD_4_SCALARMULTI_MATR_H

#include <stdio.h>
#include <fstream>

using namespace std;

#define col_el(M, i) ((M)->A[(i)])
#define mtr_size(A) ((A)->rows * (A)->columns * sizeof(cell_t))
#define mtr_el(M, i, j) ((M)->orientation ? ((M)->A[(i) * (M)->columns + (j)]) : ((M)->A[(j) * (M)->columns + (i)]))
#define mtr_pel(M, i, j)  ((M)->orientation ? &((M)->A[(i) * (M)->columns + (j)]) : &((M)->A[(j) * (M)->rows + (i)]))
#define get_rows(M) ((M)->orientation ? (M)->rows : (M)->columns)
#define get_columns(M) ((M)->orientation ? (M)->columns : (M)->rows)

typedef double cell_t;

typedef struct {
    int orientation;
    int rows;
    int columns;
    cell_t * A;
} mtr_t;


cell_t mtrEl(mtr_t * M, int i, int j);
mtr_t * readFrom(FILE * in);
mtr_t * readFrom(ifstream & in);
void    writeTo(FILE * out, mtr_t * m);
void   writeTo(ofstream & out, mtr_t * m);
mtr_t * trans(mtr_t * mtr);
cell_t scalarMul(mtr_t * A, mtr_t * B, int row, int column);
void mtrMul(mtr_t * A, mtr_t * B, mtr_t * res);
mtr_t * createMtr(int rows, int columns);
mtr_t * getCopyMtr(mtr_t * M);
mtr_t * mtrCopy (mtr_t * A, mtr_t * res);
cell_t norm(mtr_t * M);
void delMtr(mtr_t * M);
void normalize(mtr_t * M);

#endif //NUMERICALMETHOD_4_SCALARMULTI_MATR_H
