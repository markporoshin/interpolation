//
// Created by Марк on 21.12.18.
//

#ifndef NUMERICALMETHOD_4_SCALARMULTI_SOLVE_G_H
#define NUMERICALMETHOD_4_SCALARMULTI_SOLVE_G_H


#include "matr.hpp"

void solve_g(mtr_t * A, mtr_t * B);

#endif //NUMERICALMETHOD_4_SCALARMULTI_SOLVE_G_H
