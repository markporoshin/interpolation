//
// Created by Марк on 12.12.18.
//

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <memory.h>
#include <math.h>
#include <fstream>
#include "matr.hpp"

using namespace std;


mtr_t * createMtr(int rows, int columns) {
    mtr_t * res = (mtr_t *)malloc(sizeof(mtr_t));
    res->rows = rows;
    res->columns = columns;
    res->A = (cell_t *)malloc(mtr_size(res));
    res->orientation = 1;
    return res;
}

cell_t mtrEl(mtr_t * M, int i, int j) {
    return mtr_el(M, i, j);
}

mtr_t * readFrom(FILE * in) {
    int N, M;
    fscanf(in, "%d%d", &N, &M);
    mtr_t * res = createMtr(N, M);
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++) {
            fscanf(in, "%lf", (res->A + i * M + j));
        }
    return res;
}


mtr_t * readFrom(ifstream & in) {
    int N, M;
    in >> N >> M;
    //fprintf(stderr, "%d %d", N, M);
    mtr_t * res = createMtr(N, M);

    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++) {
            //in >> *(res->A + i * M + j);
            in >> res->A[i*res->columns+j];
            //fprintf(stderr, "read %d\n", i*res->columns+j);
        }
    return res;
}

void   writeTo(FILE * out, mtr_t * m) {
    fprintf(out, "%d %d\n", get_rows(m), get_columns(m));
    for (int i = 0; i < get_rows(m); i++, fprintf(out, "\n"))
        for (int j = 0; j < get_columns(m); j++) {
            fprintf(out, "%g ", (m->A[m->columns * i + j]));
        }
    fflush(out);
}

void   writeTo(std::ofstream & out, mtr_t * m) {
    out << m->rows << ' ' << m->columns << endl;
    for (int i = 0; i < get_rows(m); i++, out << '\n')
        for (int j = 0; j < get_columns(m); j++) {
            out << (float)(m->A[m->columns * i + j]);
        }
}

mtr_t * trans(mtr_t * mtr) {
    mtr_t * res = createMtr(mtr->columns, mtr->rows);
    for (int i = 0; i < mtr->rows; ++i) {
        for (int j = 0; j < mtr->columns; ++j) {
            *mtr_pel(res, j, i) = mtr_el(mtr, i, j);
        }
    }
    return res;
}

cell_t scalarMul(mtr_t * A, mtr_t * B, int row, int column) {
    assert(get_columns(A) == get_rows(B));

    cell_t res = 0;
    for (int i = 0; i < get_columns(A); i++) {
        res += mtr_el(A, row, i) * mtr_el(B, i, column);
    }
    return res;
}

void mtrMul(mtr_t * A, mtr_t * B, mtr_t * res) {
    for (int i = 0; i < get_rows(A); ++i) {
        for (int j = 0; j < get_columns(B); ++j) {
            *(mtr_pel(res, i, j)) = scalarMul(A, B, i, j);
        }
    }
}

cell_t norm(mtr_t * M) {
    cell_t res = 0;
    for (int i = 0; i < get_rows(M); ++i) {
        for (int j = 0; j < get_columns(M); ++j) {
            res += (mtr_el(M, i, j)) * (mtr_el(M, i, j));
        }
    }
    return sqrt(res);
}

cell_t normInf(mtr_t * M) {
    cell_t res = 0;
    for (int i = 0; i < get_rows(M); ++i) {
        for (int j = 0; j < get_columns(M); ++j) {
            if(res < (mtr_el(M, i, j)))
                res = mtr_el(M, i, j);
        }
    }
    return sqrt(res);
}

mtr_t * getCopyMtr(mtr_t * M) {
    mtr_t * res = createMtr(get_rows(M), get_columns(M));
    memcpy(res->A, M->A, mtr_size(M));
    res->orientation = 1;
    return res;
}

mtr_t * mtrCopy (mtr_t * A, mtr_t * res) {
    memcpy(res->A, A->A, A->columns * A->rows * sizeof(double));
    return res;
}

void delMtr(mtr_t * M) {
    free(M->A);
    free(M);
}

void normalize(mtr_t * M) {
    cell_t n = normInf(M);
    for (int i = 0; i < get_rows(M); ++i) {
        for (int j = 0; j < get_columns(M); ++j) {
            *mtr_pel(M, i, j) /= n;
        }
    }
}




//3 4 5
//3 4 2
//9 16 10