//
// Created by Марк on 21.12.18.
//

#include <math.h>
#include "solve_g.h"
#include "matr.hpp"



static int check(mtr_t * A, mtr_t * B, mtr_t * X) {
    const double E = 0.00000001;
    mtr_t * mul = getCopyMtr(B);
    mtrMul(A, X, mul);

    for (int i = 0; i < X->rows; ++i) {
        if (fabs(mtr_el(X, i, 0) - mtr_el(mul, i, 0)) > E) {
            delMtr(mul);
            return 0;
        }
    }
    delMtr(mul);
    return 1;
}
static void prepareRow(mtr_t * A, mtr_t * B, int row) {
    cell_t k = mtr_el(A, row, row);
    *mtr_pel(B, row, 0) /= k;
    for (int i = row; i < A->rows; ++i) {
        *mtr_pel(A, row, i) /= k;
    }
}
static void multiRow(mtr_t * A, int row, cell_t k) {
    for (int i = 0; i < A->columns; ++i) {
        *mtr_pel(A, row, i) *= k;
    }
}
static void subRow(mtr_t * A, int r1, int r2) {
    for (int i = 0; i < A->columns; ++i) {
        *mtr_pel(A, r2, i) -= mtr_el(A, r1, i);
    }
}
static void rowStep(mtr_t * A, mtr_t * B, int r1, int r2) {
    const double E = 0.00000001;
    if (fabs(mtr_el(A, r2, r1)) < E ) {
        return;
    }

    cell_t k = 1. / mtr_el(A, r2, r1);
    multiRow(A, r2, k);
    subRow(A, r1, r2);
    multiRow(B, r2, k);
    subRow(B, r1, r2);
}

void solve_g(mtr_t * A, mtr_t * B) {
    mtr_t * bA = getCopyMtr(A);
    mtr_t * bB = getCopyMtr(B);

    //right step for g_method
    for(int i = 0; i < A->rows; i++) {
        prepareRow(bA, bB, i);
        for (int j = i+1; j < A->rows; j++) {
            rowStep(bA, bB, i, j);
        }
    }

    //revers step for g_method
    for (int k = A->columns-1; k >= 0 ; k--) {
        prepareRow(bA, bB, k);
        for (int j = k-1; j >= 0; j--) {
            rowStep(bA, bB, k, j);
        }
    }

    if(!check(A, B, bB))
        ;//fprintf(stderr, "matr was solved incorrectly");

    FILE * slae = fopen("/Users/mark/CLionProjects/NumMethods2/slae.txt", "w");
    writeTo(slae, A);
    writeTo(slae, B);
    writeTo(slae, bB);
    mtrCopy(bB, B);
    delMtr(bB);
    delMtr(bA);
}
